# vendor_xiaomi_surya-miuicamera
### How to use?
1. clone this repo use this command
```
git clone -b surya https://gitlab.com/HinohArata/vendor_xiaomi_camera.git vendor/xiaomi/surya-miuicamera
```
2. Add this to `device.mk`
```
# Miui Camera
$(call inherit-product-if-exists, vendor/xiaomi/camera/miuicamera.mk)
```
3. Add this to `BoardConfig.mk`
```
# Inherit from proprietary files for miuicamera
-include vendor/xiaomi/surya-miuicamera/products/board.mk
```